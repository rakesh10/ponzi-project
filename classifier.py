import urllib
import pandas as pd
from bs4 import BeautifulSoup
from sklearn.naive_bayes import GaussianNB
from sklearn.feature_extraction.text import TfidfVectorizer

#Preprocessing
df = pd.read_csv("List.csv",index_col=None,header=0)
df=df.dropna()
current_score = 0
ss= ""
#print (df)
input = "https://www.google.com/"

url = input
# print (url)
html = urllib.urlopen(url).read()
soup = BeautifulSoup(html)
for script in soup(["script", "style"]):
    script.extract()
text = soup.get_text()
lines = (line.strip() for line in text.splitlines())
chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
text = '\n'.join(chunk for chunk in chunks if chunk)



for i in range(0,len(df)):

    url1 = (df.iloc[i,0])
    print("comapring "+url+" with "+ url1)
    html1 = urllib.urlopen(url1).read()
    soup1 = BeautifulSoup(html1)
    for script in soup1(["script", "style"]):
        script.extract()
    text1 = soup1.get_text()
    lines = (line.strip() for line in text1.splitlines())
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    text1 = '\n'.join(chunk for chunk in chunks if chunk)
    vect = TfidfVectorizer(min_df=1)
    tfidf = vect.fit_transform([text, text1])
    array = (tfidf * tfidf.T).A
    if df.iloc[i, 1]==1:
     current_score = (current_score + (array[1][0]))
    else:
        if array[1][0] > 0.35:
            current_score = (current_score - (array[1][0]))
        else:
            current_score = (current_score + (array[1][0]))
    print("Similarity Score is ")
    print (array[1][0])

finalScore = current_score/(i+1)
print ("Final Socre is")
print (finalScore)



Data=df[["S-Score"]]
Target=df[["isPonzi"]]
data = [finalScore]
gnb = GaussianNB()
y_pred = gnb.fit(Data,Target).predict(data)
if (y_pred[0]==1) :
    print (url + "is ponzi")
else :
    print (url + " is not ponzi")


