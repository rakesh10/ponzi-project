
### What is this repository for? ###
This project is a part of CNT-5410 Computer Network Security Course. The project aims at classifying a given site as
Ponzi or Non Ponzi using a machine learning approach

### How do I get set up? ###

You need python 2.0 or above in your computer

Dependencies
Pandas, beautifulSoup, sklearn.naive_bayes and sklearn.feature_extraction.text
